<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" import="java.io.*, java.net.*"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Basic Form</title>
</head>
<body>
<table cellspacing="10" align="left">
	<th>
<tr>
	<td align="left">
		<%
		String fileName = "/WEB-INF/builddetail.txt";
		InputStream ins = application.getResourceAsStream(fileName);
		try
	{
		if(ins == null)
		{
			response.setStatus(response.SC_NOT_FOUND);
		}
		else
		{
			BufferedReader br = new BufferedReader((new InputStreamReader(ins)));
			String data;
			while((data= br.readLine())!= null)
			{
				String[] pv = data.split("=");
				out.print(pv[1]);
			}
		} 
	}
	catch(IOException e)
		{
			out.println(e.getMessage());
		}
	%>
	</td>
</tr>
</th>	
</table>	
<table cellspacing="10" align="center">
<form action="process.jsp" method="GET">
<tr>
	<td>
		First Name:
	</td>
	<td>
		<input type="text" name="fname">
	</td>
</tr>
<tr>
	<td>
		Last Name: 
	</td>
	<td>
		<input type="text" name="lname" />
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
		<input type="submit" value="Submit" />
	</td>
</tr>
</table>
</form>
</body>
</html>
